
#############################################################################
# ROOT MODULE
#############################################################################


variable "component_name" {
  type = string
  description = "please enter the name of the component" # Mandatory
}

variable "vpc_cidr" {
  type = string
  description =  "vpc cidr for the network"
}

# optional
variable "enable_dns_hostnames" {
  type = bool
  description = "enable dns hostnames"
  default = true
}

# optional "should have default. You will call this variable in the child module if we want to overide the "true"
variable "enable_dns_support" {
  type = bool
  description = "enable dns support"
  default = true
}

variable "public_subnetcidr" {
  type = list
  description = "public subnetcidr for the network" # This is optional so default value can be empty
  default = []
}

variable "private_subnetcidr" {
  type = list
  description = "private subnetcidr for the network"
  default = []
}

variable "database_subnetcidr" {
  type = list
  description = "database subnetcidr for the network"
  default = []
}

variable "availability_zone" {
  type = list
  description = "availability zone for the network"
  default = []
}




