# operational-environment-networking-tf

This module deploys vpc and all it's components with infrastructure best practices.

# Usage

```
module "vpc" {
  source = "git::https://gitlab.com/alex.aduobe/operational-environment-networking-tf.git"

  component_name = "testing-module" 
  vpc_cidr = "10.0.0.0/16"
  availability_zone = ["us-east-1a", "us-east-1b"]
  public_subnetcidr = ["10.0.0.0/24", "10.0.2.0/24", "10.0.4.0/24"]
  private_subnetcidr = ["10.0.1.0/24", "10.0.3.0/24", "10.0.5.0/24"]
  database_subnetcidr = ["10.0.51.0/24", "10.0.53.0/24", "10.0.55.0/24"]

}
```
